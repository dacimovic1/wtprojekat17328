//admin username:admin, password:aaab
//deployana verzija na linku https://wtprojekat17328.herokuapp.com/
const express = require('express');
const bodyParser = require("body-parser");
const fs = require("fs");
const http= require("http");
const app = express() ;
const Sequelize = require('sequelize');
const sequelize = require('./models/baza.js');
const bcrypt = require('bcrypt');
const saltRounds = 10 ;
const pug = require('pug');

const session = require("express-session");

const Role = sequelize.import(__dirname+'/models/role.js');
const Korisnik = sequelize.import(__dirname+'/models/korisnik.js');
const LicniPodaci = sequelize.import(__dirname+'/models/licniPodaci.js');

app.use(session({
   secret: 'mojaSifra',
   resave: true,
   saveUninitialized: true
}));



Role.sync().then(par=>{
  Korisnik.sync().then(par1=>{
    LicniPodaci.sync().then(p=>{
      Role.findOrCreate({where:{role:"admin"}}).spread((u, created) =>{
        bcrypt.hash("aaab", saltRounds, function(err, hash) {
          console.log("Usao sam ovdje");
          Korisnik.findOne({where:{username:"admin"}}).then(result=>{
            if(result == null){

                Korisnik.create({username:"admin", password:hash, fk_roleId:u.id});

            }
          });
        });
      });

      Role.findOrCreate({where:{role:"nastavnik"}});
      Role.findOrCreate({where:{role:"student"}});



    });
  });
});



app.use(express.static(__dirname));

app.get('/stranica', function(req, res){
 let tijelo = req.query.ime;

  fs.readFile('view/'+tijelo+".html", function(err, html){
    if (err){
      throw err;
    }

    res.writeHeader(200, {"Content-Type": "text/html"});
    res.write(html);
    res.end();
  });
});

function provjeriRole(){
  console.log("pozvao sam provjeravanje rola");

}

app.get('', function(req, res){


  fs.readFile('index.html', function(err, html){
    if (err){
      throw err;
    }
    res.writeHeader(200, {"Content-Type": "text/html"});
    res.write(html);
    res.end();
  });
});
app.get('/', function(req, res){
  provjeriRole();
  fs.readFile('index.html', function(err, html){
    if (err){
      throw err;
    }
    res.writeHeader(200, {"Content-Type": "text/html"});
    res.write(html);
    res.end();
  });
});


app.use(bodyParser.json());
app.post('/unosSpiska', function(req, res){

    if(req.session.role != 2)
    {
      res.setHeader('Content-Type', 'application/json');
      res.json({message:"Greska, neovlasten pristup"});
    }else{
      let br = req.query.brojSpirale ;
      console.log(req.url);
      let tijelo = req.body;

      fs.writeFile('spisakS'+br.toString()+'.json',JSON.stringify(tijelo)  , function (err) {

        if (err) throw err;

        res.setHeader('Content-Type', 'application/json');
        res.json({message:"Uspješno dodan red",data:tijelo});
      });
    }
});

function ispitajSadrzaj(sadrzaj){
  for(var i=0 ; i < sadrzaj.length; i++)
  {
    if(!(sadrzaj[i].hasOwnProperty('sifra_studenta') && sadrzaj[i].hasOwnProperty('tekst') && sadrzaj[i].hasOwnProperty('ocjena')))
      return false
  }
  return true;
};

app.post('/komentar', function(req, res){

  if(req.session.role != 'student')
  {
    res.setHeader('Content-Type', 'application/json');
    res.json({message:"Greska, neovlasten pristup"});
  }else
  {
    let tijelo = req.body;
    console.log(tijelo);
    if(tijelo.hasOwnProperty('spirala') == false || tijelo.hasOwnProperty('index') == false ||  tijelo.hasOwnProperty('sadrzaj')==false || tijelo['spirala'].length ==0 || tijelo['index'].length==0 || tijelo['sadrzaj'].length==0){
      res.setHeader('Content-Type', 'application/json');
      res.json({ "message":"Podaci nisu u traženom formatu!",data:null});
    }else{

      fs.writeFile('marksS'+tijelo['spirala']+tijelo['index']+'.json',JSON.stringify(tijelo['sadrzaj'])  , function (err) { //mozda treba stringify uraditi nad parametrima

              if (err) throw err;
              res.setHeader('Content-Type', 'application/json');
              res.json({"message":"Uspješno kreirana datoteka!","data": tijelo['sadrzaj']});
      });
    }
  }
});

app.post('/lista', function(req, res){
  if(req.session.role != 'nastavnik')
  {
    res.setHeader('Content-Type', 'application/json');
    res.json({message:"Greska, neovlasten pristup"});
  }else{
    let tijelo = req.body;

    if(tijelo.hasOwnProperty('godina') == false || tijelo['godina']==null || tijelo.hasOwnProperty('nizRepozitorija') == false || tijelo['nizRepozitorija']==null)
    {
      res.setHeader('Content-Type', 'application/json');
      res.json({ "message":"Podaci nisu u traženom formatu!",data:null});
    }else{
        var listaRepozitorija = tijelo['nizRepozitorija'];
        var upisuj = "";
        var brojRedova = 0;
        for (var i=0; i<listaRepozitorija.length; i++)
        {
          if(listaRepozitorija[i].indexOf(tijelo['godina']) != -1)
          {
            upisuj+=listaRepozitorija[i]+"\r\n";
            brojRedova += 1;
          }
        }

        fs.writeFile('spisak'+tijelo['godina']+'.txt',upisuj  , function (err) {

                if (err) throw err;

                res.setHeader('Content-Type', 'application/json');
                res.json({"message":"Lista uspješno kreirana","data":brojRedova});
      });
    }
  }
});
app.post('/izvjestaj', function(req, res){
  if(req.session.role != 'nastavnik')
  {
    res.setHeader('Content-Type', 'application/json');
    res.json({message:"Greska, neovlasten pristup"});
  }else{
  let tijelo = req.body;

  fs.readFile('spisakS'+tijelo['spirala']+'.json','utf8', (err, data) => {
    if (err) throw err;

    var spisak = JSON.parse(data);

    var mapa = ["A","B","C","D","E"];

    var zaPretragu= new Array();
    var brojac = 0 ;

    for(var i=0; i<spisak.length; i++){

      for(var j=1; j<6;j++){

        if(tijelo['index']==spisak[i][j]){
          zaPretragu[brojac++]= {'index':spisak[i][0], "pozicija":mapa[j-1] };
        }
      }
    }


    var komentari = "";

    fs.readdir(__dirname, (errr, list) =>{
      if (errr) throw errr;
      for(var i=0; i<zaPretragu.length;i++)
      {
        for(var j=0; j<list.length; j++){

          if(list[j] == "marksS"+tijelo['spirala']+zaPretragu[i]['index']+".json"){
              var sad = fs.readFileSync(list[j], 'utf8');


              var fin = JSON.parse(sad);



              for(var k=1; k<fin.length; k++)
              {
                if(fin[k].sifra_studenta == zaPretragu[i]['pozicija']){
                  komentari += fin[k].tekst+"\r\n"+"##########"+"\r\n";
                }
              }

          }
        }
      }
      fs.writeFile('izvjestajS'+tijelo['spirala']+tijelo['index']+'.txt',komentari , function (err) {

              if (err) throw err;
              res.setHeader('Content-Type', 'application/json');
              var vrijednost = JSON.stringify('izvjestajS'+tijelo['spirala']+tijelo['index']+'.txt');
              vrijednost = vrijednost.substring(1,vrijednost.length-1);
              res.json({"message":"Lista uspješno kreirana", 'data':komentari, 'ime':vrijednost});
      });

    });
  });
}
});
app.post('/bodovi', function(req, res){
  if(req.session.role != 'nastavnik')
  {
    res.setHeader('Content-Type', 'application/json');
    res.json({poruka:"Greska, neovlasten pristup"});
  }else{
  let tijelo = req.body;

  fs.readFile('spisakS'+tijelo['spirala']+'.json','utf8', (err, data) => {
    if (err) throw err;

    var spisak = JSON.parse(data);

    var mapa = ["A","B","C","D","E"];

    var zaPretragu= new Array();
    var brojac = 0 ;

    for(var i=0; i<spisak.length; i++){

      for(var j=1; j<6;j++){

        if(tijelo['index']==spisak[i][j]){
          zaPretragu[brojac++]= {'index':spisak[i][0], "pozicija":mapa[j-1] };
        }
      }
    }


    var suma= 0;

    fs.readdir(__dirname, (errr, list) =>{
      if (errr) throw errr;
      for(var i=0; i<zaPretragu.length;i++)
      {
        for(var j=0; j<list.length; j++){

          if(list[j] == "marksS"+tijelo['spirala']+zaPretragu[i]['index']+".json"){
              var sad = fs.readFileSync(list[j], 'utf8');


              var fin = JSON.parse(sad);



              for(var k=1; k<fin.length; k++)
              {
                if(fin[k].sifra_studenta == zaPretragu[i]['pozicija']){
                  suma += fin[k].ocjena;
                }
              }

          }
        }
      }
      console.log(suma);
      console.log(brojac);

      var prosjek = Math.trunc(suma/brojac) + 1 ;
      console.log(prosjek);
              res.setHeader('Content-Type', 'application/json');

              res.json({'poruka':'Student '+tijelo['index']+' je ostvario u prosjeku '+prosjek+' mjesto'});


    });
  });
}
});

app.post("/registrujStudenta", function(req,res){
    let tijelo = req.body;

    bcrypt.hash(tijelo.password, saltRounds, function(err, hash) {

      Korisnik.findOne({where:{username:tijelo.imePrezime}}).then(result => {
        if(result === null){
          Role.findOne({where:{role: "student"}}).then(role=>{
            Korisnik.create({ username :tijelo.imePrezime,
                          password :hash,
                          verified : null,
                          fk_roleId: role.id}).then(korisnik=>{

                            LicniPodaci.create({
                              imeiPrezime:tijelo.imePrezime,
                              brojIndexa:parseInt(tijelo.brojIndexa),
                              borjGrupe:parseInt(tijelo.brojGrupe),
                              akademskaGodina:tijelo.akademskaGodina,
                              bitbucketURL:tijelo.bitbucketURL,
                              bitbucketSSH:tijelo.bitbucketSSH,
                              nazivRepozitorija:tijelo.nazivRepozitorija,
                              email:null,
                              maxBrojGrupa:null,
                              regex:null,
                              ljetniSemestar:null,
                              fk_korisnikId:korisnik.id
                            }).then(licniPodaci =>{
                              res.writeHeader(200, {"Content-Type": "text/html"});
                              res.write(JSON.stringify(licniPodaci.toJSON()));
                              res.end();
                            });
                          });
                        });
        }else {
          res.writeHeader(200, {"Content-Type": "text/html"});
          res.write("Vec postoji korisnik sa istim usernameom");
          res.end();
        }
    });
    });

});

app.post("/registrujProfesora", function(req,res){
  let tijelo = req.body;

  bcrypt.hash(tijelo.password, saltRounds, function(err, hash) {
    Korisnik.findOne({where:{username:tijelo.korisnickoIme}}).then(result => {
      if(result === null){
        Role.findOne({where:{role: "nastavnik"}}).then(role=>{
          Korisnik.create({ username :tijelo.korisnickoIme,
                        password :hash,
                        verified : false,
                        fk_roleId: role.id}).then(korisnik=>{

                          LicniPodaci.create({
                            imeiPrezime:tijelo.imePrezime,
                            brojIndexa:null,
                            borjGrupe:null,
                            akademskaGodina:null,
                            bitbucketURL:null,
                            bitbucketSSH:null,
                            nazivRepozitorija:null,
                            email:tijelo.email,
                            maxBrojGrupa:tijelo.maxBrojGrupa,
                            regex:tijelo.regex,
                            ljetniSemestar:tijelo.ljetniSemestar,
                            fk_korisnikId:korisnik.id
                          }).then(licniPodaci =>{
                            res.writeHeader(200, {"Content-Type": "text/html"});
                            res.write(JSON.stringify(licniPodaci.toJSON()));
                            res.end();
                          });
                        });
                      });
      }else {
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.write("Vec postoji korisnik sa istim usernameom");
        res.end();
      }
    });
  });
});

app.post('/login', function(req, res){
    var tijelo = req.body;
    Korisnik.findOne({where:{username:tijelo.username}}).then(result=>
    {
        if(result != null){
            console.log("Hesirani pass je "+result.password);
            console.log("Nehesirani je "+tijelo.password);
            bcrypt.compare(tijelo.password, result.password).then(function(ress) {
              console.log(ress);
              if(ress===true && result.verified != false)
              {
                console.log("ress");
                var stranica = '';

                Role.findById(result.fk_roleId).then(role =>{
                  stranica= '';
                  if(role.role == 'admin'){
                    stranica  = 'listaKorisnika';
                  }else {
                    stranica = role.role;
                  }
                  fs.readFile('view/'+stranica+".html", function(err, html){
                    if (err){
                      throw err;
                    }
                    req.session.role = role.role;
                    console.log("Rola je : "+req.session.role);
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                  });
                });

              }else
              if(ress === false){
                console.log("ressasdfasdfasdf");
                res.writeHeader(400, {"Content-Type": "text/html"});
                res.write("Neispravan password");
                res.end();
              }else if(result.verified == false)
              {
                console.log("ressasdfasdfasdf");
                res.writeHeader(400, {"Content-Type": "text/html"});
                res.write("Neverifikovan profil");
                res.end();
              }
            });
          } else {
          res.writeHeader(400, {"Content-Type": "text/html"});
          res.write("Nepostojeci korisnik");
          res.end();
          }



    });
});
app.post('/logout', function(req, res){

  req.session.destroy(function(err){
    if (err){
      throw err;
    }
  });
  fs.readFile('view/'+'logout'+".html", function(err, html){


    res.writeHeader(200, {"Content-Type": "text/html"});
    res.write(html);
    res.end();
  });
});

app.post('/listaKorisnika', function(req,res){
  if(req.session.role != 'admin'){
    res.writeHeader(400, {"Content-Type": "text/html"});
    res.write("Nedozvoljen pristup");
    res.end();
  }else {
    var tijelo = req.body.poruka;
    Role.findOne({where:{role:"nastavnik"}}).then(nastavnik=>{
      if(tijelo == ''){
        Korisnik.findAll().then(rez=>{

          res.setHeader('Content-Type', 'application/json');

          res.json(rez);

        });
      }else{
        Korisnik.findAll({where:{username: tijelo}}).then(rez=>{

          res.setHeader('Content-Type', 'application/json');

          res.json(rez);


        });
      }
    });

  }
});

app.listen(3000);
