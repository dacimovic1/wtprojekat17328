const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const LicniPodaci = sequelize.define('licniPodaci', {
    imeiPrezime:Sequelize.STRING,
    brojIndexa:Sequelize.INTEGER,
    borjGrupe:Sequelize.INTEGER,
    akademskaGodina:Sequelize.STRING,
    bitbucketURL:Sequelize.STRING,
    bitbucketSSH:Sequelize.STRING,
    nazivRepozitorija:Sequelize.STRING,
    email:Sequelize.STRING,
    maxBrojGrupa:Sequelize.INTEGER,
    regex:Sequelize.STRING,
    ljetniSemestar:Sequelize.BOOLEAN
});

const Korisnik = sequelize.import(__dirname+'/korisnik.js');
LicniPodaci.belongsTo(Korisnik, {foreignKey: "fk_korisnikId"});

module.exports = function(sequelize, DataTypes){
  return LicniPodaci;
}
