const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const Korisnik = sequelize.define('korisnik', {
      username: {
        type: Sequelize.STRING
    },
    password: {
      type:Sequelize.STRING
    },
    verified:Sequelize.BOOLEAN
  });

const Role = sequelize.import(__dirname+'/role.js');
Korisnik.belongsTo(Role,  {foreignKey: "fk_roleId"});

module.exports = function(sequelize, DataTypes){
  return Korisnik;
}
