const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const Role = sequelize.define('role', {
      role: Sequelize.STRING
});

module.exports = function(sequelize, DataTypes){
  return Role;
}
