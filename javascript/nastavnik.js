function generisiIzvjestaj()
{
  var index= document.getElementById('index').value;
  var spirala= document.getElementById('spirala').value;

  KreirajFajl.kreirajIzvjestaj(spirala, index, callBackNastavnik);

}

function dajBodove(){
  var index= document.getElementById('index').value;
  var spirala= document.getElementById('spirala').value;

  KreirajFajl.kreirajBodove(spirala, index, ispisBodova); 
}

function ispisBodova(error, tekst){
  document.getElementById('sadrzaj').innerHTML = tekst;
}

function callBackNastavnik(error, teks)
{
  try {
    tekst = JSON.parse(teks);

    var element = document.createElement('a');
   element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(tekst['data']));
   element.setAttribute('download', tekst['ime']);

   element.style.display = 'none';
   document.body.appendChild(element);

   element.click();

   document.body.removeChild(element);
  } catch (e) {

  } finally {

  }
}
