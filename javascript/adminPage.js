function pretraga(){
  var poruka = {poruka: document.getElementById('tekstPretrage').value};
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function(){
    if(ajax.status == 200 && ajax.readyState == 4){
        ispisi(ajax.responseText);
    }
  }
  ajax.open("POST", "/listaKorisnika", true);
  ajax.setRequestHeader("Content-Type", "application/json");
  ajax.send(JSON.stringify(poruka));
}
