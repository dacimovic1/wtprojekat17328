function pomjeriGore(dugme)
{
    var red = dugme.parentNode.parentNode;
    if(red.rowIndex == 1) return;

    var tabela= red.parentNode;

    var pomocniRed = tabela.rows[red.rowIndex-1].innerHTML;
    tabela.rows[red.rowIndex-1].innerHTML = tabela.rows[red.rowIndex].innerHTML;
    tabela.rows[red.rowIndex].innerHTML=pomocniRed;
}

function pomjeriDole(dugme)
{
    var red = dugme.parentNode.parentNode;
    if(red.rowIndex == 5) return;

    var tabela= red.parentNode;

    var pomocniRed = tabela.rows[red.rowIndex+1].innerHTML;
    tabela.rows[red.rowIndex+1].innerHTML = tabela.rows[red.rowIndex].innerHTML;
    tabela.rows[red.rowIndex].innerHTML=pomocniRed;
}

function potvrdiUnos(){
  var brojSpirale = document.getElementById("brojSpirale").value ;
  var brojIndexa = document.getElementById("brojIndexa").value;

  var jsonSadrzaj = new Array();
  var tabela = document.getElementById("tabela");
  for(var i=1; i<=5;i++){
    jsonSadrzaj[i-1] = {'sifra_studenta':tabela.rows[i].cells[1].innerHTML,'tekst':tabela.rows[i].cells[3].children[0].value, 'ocjena':i-1};
  }

  KreirajFajl.kreirajKomentar(brojSpirale.toString(), brojIndexa.toString(), jsonSadrzaj, callBackKomentari );
}

function callBackKomentari(status, response)
{
  document.getElementById('sadrzaj').innerHTML += response;
}
