var Poruke=(function(){
  var idDivaPoruka;
  var mogucePoruke=["Email koji ste napisali nije validan fakultetski email",
  "Indeks kojeg ste napisali nije validan",
  "Nastavna grupa koju ste napisali nije validna",
  "Akademska godina koju ste napisali nije validna",
  "Password koji ste napisali nije validan",
  "Ponovljeni password nije isti kao i prvouneseni",
  "BitbucketURL koji ste napisali nije validan",
  "BitbucketSSH koji ste napisali nije validan",
  "Naziv repozitorija koji ste napisali nije validan",
  "Ime i prezime koje ste napisali nisu validni"];
  var porukeZaIspis=[];

  var postaviIdDiva = function(nazivDiva)
  {
    idDivaPoruka = nazivDiva;
  }

  var dodajPoruku= function(brojPoruke){
    if(!porukeZaIspis.includes(mogucePoruke[brojPoruke])){

      porukeZaIspis.push(mogucePoruke[brojPoruke]);
    }

  }

  var ocistiGresku= function(brojPoruke)
  {
    if(porukeZaIspis.includes(mogucePoruke[brojPoruke])){
      porukeZaIspis.splice(porukeZaIspis.indexOf(mogucePoruke[brojPoruke]), 1);
    }
  }

  var ispisiGreske= function()
  {
    var x=document.getElementById(idDivaPoruka);
    if(x != null){
        x.innerHTML= "";

      for(i=0; i<porukeZaIspis.length; i++)
      {
        x.innerHTML+=porukeZaIspis[i]+"<br>";

      }
    }
  }

  var ocistiSveGreske = function(){
    var x=document.getElementById(idDivaPoruka);
    if(x != null){
        x.innerHTML= "";
    }
    porukezaIspis = [];
  }

  return{
  ispisiGreske: ispisiGreske,
  postaviIdDiva: postaviIdDiva,
  dodajPoruku: dodajPoruku,
  ocistiGresku: ocistiGresku,
  ocistiSveGreske: ocistiSveGreske
  }
}());
