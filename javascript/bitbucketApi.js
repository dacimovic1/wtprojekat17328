var BitbucketApi = (function(){
    var refreshToken='';
    var tajna = '';
    var kljuc= '';
    var postoji = false;
    function metoda(er, ima)
    {
      if(er == null) {
        postoji = ima;
        //console.log("Promjenjen parametar u:"+ima);
      }
    }

    return {
        dohvatiAccessToken: function(key, secret, fnCallback){
          tajna = secret ;
          kljuc = key;
          if(key === null || secret === null){
            fnCallback(-1, "Key ili secret nisu pravilno proslijeđeni!");

          }else{
            var ajax= new XMLHttpRequest();

            ajax.onreadystatechange = function()
            {

              if(ajax.readyState == 4 && ajax.status == 200)
              {

                refreshToken=JSON.parse(ajax.responseText).refresh_token;
                console.log(refreshToken);
                fnCallback(null, JSON.parse(ajax.responseText).access_token);
              }else {
                fnCallback(ajax.status, ajax.responseText);
              }
            }

              ajax.open("POST", "https://bitbucket.org/site/oauth2/access_token", true);
              ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
              ajax.setRequestHeader("Authorization", 'Basic ' + btoa(key+":"+secret));
              ajax.send("grant_type="+encodeURIComponent("client_credentials"));
            }

        },
        dohvatiRepozitorije: function(token, godina, naziv, branch, fnCallback){
          var ajax = new XMLHttpRequest();
          var response = [];
          var promises = [];
          ajax.onreadystatechange = function()
          {

            if(ajax.readyState == 4 && ajax.status == 200){

              var podaci = JSON.parse(ajax.responseText);

              for(var i=0; i<podaci.values.length; i++)
              {
                var datum = new Date(podaci.values[i].created_on);
                var godinaRepozitorija = datum.getFullYear();
                if( godinaRepozitorija == godina || godinaRepozitorija == String(parseInt(godina)+1)){
                  promises.push(BitbucketApi.dohvatiBranch(token, podaci.values[i].links.branches.href, branch, metoda));
                }
              }
                var results = Promise.all(promises);
                console.log(results);

                results.then(function(data){
                  console.log(data);
                  brojac = 0 ;
                    for(var i=0; i< data.length; i++)
                    {
                      for(var j=0;j<podaci.values[i].links.clone.length; j++)
                      {
                        if(podaci.values[i].links.clone[j].name == 'ssh' && data[i]){
                          response.push(podaci.values[i].links.clone[j].href);

                        }
                      }
                    }
                    fnCallback(null, response);
                });



              }else{
              fnCallback(ajax.status, ajax.responseText);
            }
          }
          var ime=encodeURIComponent("~\""+naziv+"\"");//trebaju li navodnici na nayivu????
          ajax.open("GET","https://api.bitbucket.org/2.0/repositories?role=member&q=name"+ime+"&pagelen=150", true);
          ajax.setRequestHeader("Authorization", 'Bearer ' + token);
          ajax.send();
        },
        dohvatiBranch: function(token, url, naziv, fnCallback){
          return new Promise(function(resolve, reject){
            var ajax = new XMLHttpRequest();
            var response=[];
            ajax.onreadystatechange = function()
            {
              if(ajax.readyState == 4 && ajax.status == 200){
                var postoji = JSON.parse(ajax.responseText).values.length;
                var exists = false;
                if(postoji != 0)
                  exists = true;

                  console.log(exists);

                  fnCallback(null, exists);
                  resolve(exists);


              }else{
                fnCallback(ajax.status, false);
              }
            }
            var i=encodeURIComponent("=\""+naziv+"\"");//mozda treba = a ne ~
            ajax.open("GET", url+'?q=name'+i, true);
            ajax.setRequestHeader("Authorization", 'Bearer ' + token);
            ajax.send();
          });

        }
    }
})();
