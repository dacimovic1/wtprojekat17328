function vidiljivostForme(a){
  Poruke.ocistiSveGreske();
  if(a == 1){
    document.getElementById("formaStudent").reset();
    document.getElementById("formaProfesor").style.display = "block";
    document.getElementById("formaStudent").style.display = "none";
    document.getElementById("registrujStudenta").style.backgroundColor="#f0b5a8";
    document.getElementById("registrujProfesora").style.backgroundColor="#e06c52";
  }else if(a == 2){
    document.getElementById("formaProfesor").reset();
    document.getElementById("formaProfesor").style.display = "none";
    document.getElementById("formaStudent").style.display = "block";
    document.getElementById("registrujStudenta").style.backgroundColor="#e06c52";
    document.getElementById("registrujProfesora").style.backgroundColor="#f0b5a8";
  }
}

function mojIspis(tekst){
  var meni = document.getElementsByClassName("meni")[0];
  meni.innerHTML = tekst;
}

function registruj(){
  if (document.getElementById("formaStudent").style.display == "none"){
    console.log("Registujemo profesora");
    registrujProfesora();
  }else{
    console.log("Registujemo Studenta");

    registrujStudenta();
  }
}

function registrujStudenta()
{
  var poruka ={
  imePrezime : document.getElementsByName("registrationS_imePrezime")[0].value,
  brojIndexa : document.getElementsByName("registrationS_index_number")[0].value,
  brojGrupe : document.getElementsByName("registrationS_group")[0].value,
  akademskaGodina : document.getElementsByName("registrationS_AkGod")[0].value,
  password : document.getElementsByName("registrationS_password")[0].value,
  bitbucketURL : document.getElementsByName("registrationS_bitbucketURL")[0].value,
  bitbucketSSH : document.getElementsByName("registrationS_bitbucketSSH")[0].value,
  nazivRepozitorija : document.getElementsByName("registrationS_nazivRepozitorija")[0].value
  }

  if(document.getElementById("errorDiv").innerHTML == ""){
    var ajax = new XMLHttpRequest();

    ajax.onreadystatechange = function(){
      if(ajax.readyState == 4 && ajax.status == 200){
        ispisi(ajax.responseText);
      }else{
        ispisi("Greska pri registraciji");
      }

    }
    ajax.open("POST", "/registrujStudenta", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(poruka));

  }else{
    alert("Neispravni parametri");
  }
}

function registrujProfesora()
{
  var poruka ={
  imePrezime : document.getElementsByName("registrationP_imePrezime")[0].value,
  korisnickoIme : document.getElementsByName("registrationP_korisnickoIme")[0].value,
  ljetniSemestar : document.getElementsByName("registrationP_Semestar")[0].selectedIndex,
  regex : document.getElementsByName("registrationP_regex")[0].value,
  password : document.getElementsByName("registrationP_password")[0].value,
  bitbucketURL : document.getElementsByName("registrationS_bitbucketURL")[0].value,
  maxBrojGrupa : document.getElementsByName("registrationP_max_broj_grupa")[0].value,
  email : document.getElementsByName("registrationP_email")[0].value
  }

  if(document.getElementById("errorDiv").innerHTML == ""){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
      if(ajax.readyState == 4 && ajax.status == 200){
        ispisi(ajax.responseText);
      }else{
        ispisi("Greska pri registraciji");
      }
    }
    ajax.open("POST", "/registrujProfesora", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(poruka));

  }else{
    alert("Neispravni parametri");
  }
}

function login(){
  var poruka = {
    username: document.getElementById("username").value,
    password: document.getElementById("password").value
  }

  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function(){
    if(ajax.status == 200 && ajax.readyState == 4){
      mojIspis(ajax.responseText);
      ispisi("");
    }else if(ajax.status ==400){
      ispisi(ajax.responseText);
    }
  }
  ajax.open("POST", "/login", true);
  ajax.setRequestHeader("Content-Type", "application/json");
  ajax.send(JSON.stringify(poruka));
}
function logout()
{
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function(){
    if(ajax.status == 200 && ajax.readyState == 4){
      mojIspis(ajax.responseText);
      ispisi("Uspjesna odjava");
    }
  }
  ajax.open("POST", "/logout", true);
  ajax.setRequestHeader("Content-Type", "application/json");
  ajax.send();
}

Poruke.postaviIdDiva("errorDiv");


function porukaMenadzer(brojPoruke, trebaUpisati)
{

  if(!trebaUpisati)
  {
    Poruke.dodajPoruku(brojPoruke);
  }else {
    Poruke.ocistiGresku(brojPoruke);
  }

  Poruke.ispisiGreske();
}

function valIme(objekat){
      var x = Validacija.validirajImeiPrezime(objekat.value);
      porukaMenadzer(9, x);
}

function valFakultetski(objekat){
      var x  = Validacija.validirajFakultetski(objekat.value);
      porukaMenadzer(0, x);
}

function valIndex(objekat){
        var x = Validacija.validirajIndex(objekat.value);
        porukaMenadzer(1, x);
}

function valGrupu(objekat){
        var x = Validacija.validirajGrupu(objekat.value);
        porukaMenadzer(2, x);
}

function valAkademskuGodinu(objekat){
      var x =Validacija.validirajAkGod(objekat.value);
      porukaMenadzer(3, x);
}

function valPassword(objekat){
      var x=Validacija.validirajPassword(objekat.value);
      porukaMenadzer(4, x);
}

function valPonovoPassword(profesor){
      if(profesor == true)
      {
        var x=Validacija.validirajPotvrdu(document.getElementsByName("registrationP_password")[0].value, document.getElementsByName("reistrationP_repeat_password")[0].value);
      }else if (profesor == false)
      {
        var x=Validacija.validirajPotvrdu(document.getElementsByName("registrationS_password")[0].value, document.getElementsByName("reistrationS_repeat_password")[0].value);
      }
      porukaMenadzer(5, x);
}

function valURL(objekat){
        var x= Validacija.validirajBitbucketURL(objekat.value);
        porukaMenadzer(6, x);
}

function valSSH(objekat){
          var x = Validacija.validirajBitbucketSSH(objekat.value);
          porukaMenadzer(7, x);
}

function valRepozitorij(objekat)
{
  var x=Validacija.validirajNazivRepozitorija(null , objekat.value);//ya sada se ispituje sa null, jer nije dobro definisan nacin provjere
  porukaMenadzer(8, x);
}

function postGrupa(objekat){
    Validacija.postaviMaxGrupa(objekat.value);
}

function postSemestar(objekat){
    Validacija.postaviTrenSemestar(objekat.selectedIndex);
}
