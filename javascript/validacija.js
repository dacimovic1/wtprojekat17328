var Validacija =(function(){
    var maxGrupa=7;
    var trenutniSemestar=0;//0 za zimski, 1 za ljetni semestar

    var postaviMaxGrupa = function(novaVrijednost)
    {
      maxGrupa= novaVrijednost;//treba li dodati validaciju?
    }

    var postaviTrenSemestar = function(novaVrijednost)
    {
      trenutniSemestar= novaVrijednost;
    }


    var validirajFakultetski = function(email){
        var re= new RegExp("^[a-zA-Z0-9!#$%&’*+/=?^_`{|}~-][a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]{0,}@etf.unsa.ba$");//ne moze poceti sa tackom

        return re.test(email);
    }

    var validirajIndex = function(index){

      var re= /^1\d{4}$/;
      return re.test(String(index)) ;
    }

    var validirajGrupu = function(brojGrupe){

        return (brojGrupe > 0 && brojGrupe<= maxGrupa);
    }

    var validirajAkGod = function(godina){
        //prvo validirajmo format
        var re= new RegExp("^(20[0-9]{2})\/(20[0-9]{2})$");

        if(re.test(godina) == false)
        {
          return false;
        }

        var prvaGodina= parseInt(godina.substring(0,4));
        var drugaGodina= parseInt(godina.substring(5));

        return (prvaGodina == (drugaGodina-1) );

    }

    var validirajPassword = function(password)
    {
      var velikoSlovo= new RegExp("[A-Z]");
      var broj= new RegExp("[0-9]");
      var maloSlovo= new RegExp("[a-z]");

      if (!velikoSlovo.test(password) || !maloSlovo.test(password) || !broj.test(password) || password.length < 7 || password.length >20)
      {
        return false ;
      }

      return true;
    }

    var validirajPotvrdu = function(password, testniPassword)
    {
      return (password === testniPassword);
    }

    var validirajBitbucketURL = function(bitbucketURL){
      var re= /^https:\/\/[a-zA-Z0-9]+@bitbucket\.org\/[a-zA-Z0-9]+\/wt[pP]rojekat1\d{4}\.git$/;

      return re.test(bitbucketURL);
    }

    var validirajBitbucketSSH = function(bitbucketSSH){
      var re=  /^git@bitbucket\.org:[a-zA-Z0-9]+\/wt[pP]rojekat1\d{4}\.git$/;

      return re.test(bitbucketSSH);
    }

    var validirajNazivRepozitorija= function(regex, naziv){
      if (regex == null){
        var re= new RegExp("^wt[p|P]rojekat1[0-9]{4}$");
        return re.test(naziv);
      }else
      {
        return regex.test(naziv);
      }

    }

    var validirajImeiPrezime = function(imePrezime)
    {
      var re=  /^([A-ZČĆŽŠĐ]['A-Za-zČčĆćŽžŠšĐđ-]{2,11})( [A-ZČĆŽŠĐ]['A-Za-zČčĆćŽžŠšĐđ-]{2,11})*$/

      return re.test(imePrezime);
    }



    return{
        validirajFakultetski: validirajFakultetski,
        validirajIndex: validirajIndex,
        validirajGrupu: validirajGrupu,
        validirajAkGod: validirajAkGod,
        validirajPassword: validirajPassword,
        validirajPotvrdu: validirajPotvrdu,
        validirajBitbucketURL: validirajBitbucketURL,
        validirajBitbucketSSH: validirajBitbucketSSH,
        validirajNazivRepozitorija: validirajNazivRepozitorija,
        validirajImeiPrezime: validirajImeiPrezime,
        postaviMaxGrupa: postaviMaxGrupa,
        postaviTrenSemestar: postaviTrenSemestar
    }
}());
