var KreirajFajl=(function(){

  var ispitajParametar = function(sadrzaj){
    for(var i=0 ; i < sadrzaj.length; i++)
    {
      if(!(sadrzaj[i].hasOwnProperty('sifra_studenta') && sadrzaj[i].hasOwnProperty('tekst') && sadrzaj[i].hasOwnProperty('ocjena')))
        return false
    }
    return true;
  };

    return {
        kreirajKomentar: function(spirala, index, sadrzaj, fnCallback){
          if ((typeof spirala === "string") && spirala.length >= 1 && (typeof index === "string") && index.length >=1 && ispitajParametar(sadrzaj)){

            var ajax = new XMLHttpRequest();

            ajax.onreadystatechange = function(){
              if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null, ajax.responseText);
              }else{
                fnCallback(ajax.status, ajax.responseText);
              }
            }

            var poruka = {'spirala': spirala,'index': index,'sadrzaj': sadrzaj}; //mozda treba sadrzaj Stringify
            console.log(poruka)

            ajax.open("POST", "http://localhost:3000/komentar", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(poruka));
          }else{
            fnCallback(-1, 'Neispravni parametri');
          }
        },
        kreirajListu : function(godina, nizRepozitorija, fnCallback){
          console.log(nizRepozitorija[0]);
          if (typeof godina=="string" && godina.length >=1 && nizRepozitorija.length > 0){

            var ajax = new XMLHttpRequest();

            ajax.onreadystatechange = function(){
              if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null, ajax.responseText);
              }else{
                fnCallback(ajax.status, ajax.responseText);
              }
            }

            var poruka = {'godina':godina, 'nizRepozitorija':nizRepozitorija}; //mozda treba sadrzaj Stringify

            ajax.open("POST", "http://localhost:3000/lista", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(poruka));
          }else{
            fnCallback(-1, 'Neispravni parametri');
          }
        },
        kreirajIzvjestaj :function(spirala,index, fnCallback){
          if (typeof index === "string" && index.length > 0){

            var ajax = new XMLHttpRequest();

            ajax.onreadystatechange = function(){
              if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null, ajax.responseText);
              }else{
                fnCallback(ajax.status, ajax.responseText);
              }
            }

            var poruka = {'spirala':spirala, 'index':index};

            ajax.open("POST", "http://localhost:3000/izvjestaj", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(poruka));
          }else{
            fnCallback(-1, 'Neispravni parametri');
          }
        },
        kreirajBodove : function(spirala,index, fnCallback){
          if (typeof index === "string" && index.length > 0){

            var ajax = new XMLHttpRequest();

            ajax.onreadystatechange = function(){
              if(ajax.readyState == 4 && ajax.status == 200){
                fnCallback(null, ajax.responseText);
              }else{
                fnCallback(ajax.status, ajax.responseText);
              }
            }

            var poruka = {'spirala':spirala, 'index':index};

            ajax.open("POST", "http://localhost:3000/bodovi", true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(poruka));
          }else{
            fnCallback(-1, 'Neispravni parametri');
          }
        }
    }
})();
