function ispisi(tekst)
{
  document.getElementById("sadrzaj").innerHTML = tekst;
}

function pokusajAjax(stranica){
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function(){
    if(ajax.readyState == 4 && ajax.status == 200){
      ispisi(ajax.responseText);
    }else{
      ispisi("Greska");
    }
  }
  ajax.open("GET", "stranica"+"?ime="+stranica, true);
  ajax.send();

}
