
 ###### 
Svi zadaci su urapeni,stranica je responsive, kod je jako hitak i jasan, nema bug-ova, CSS je validiran i hitljiv, kod se ne ponavlja, koriten je common.css. Zamjerke su kod validacije HTML-a pojavljuje se Error - The character encoding was not declared. Proceeding using windows-1252. Dizajn je veoma jednostavan.

 ###### 
tekst komentara za projekat D,razdvojen css od html-a, kod validiran ,los izbor boja, u textarea nije koristen placeholder(potrebno izbrisati tekst)

 ###### 
Funkcionalnosti tražene u postavci zadaće su ispunjene. U HTML fajlovima character encoding nije definisan, pa se koristi default-ni. Izuzev toga, sve je validno u HTML fajlovima. CSS fajlovi su odrađeni bez greške, kod je uredno napisan, i pregledljiv. Dizajn stranice koristi neobične boje, pri čemu ova kombinacija boja jako dobro izgleda. Jedine primjedbe koje imam su "Studnet" i "Ocijena" u unoskomentara.html, i što ima jedan višak button u statistika.html(button sa tekstom 5).

 ###### 
Sve stranice se jako dobro prikazuju na razlicitim sirinama. Postoji dosljednost u stilu, slicni elementi izgledaju slicno na svim stranicama. Kod je citljiv i ne ponavlja se. Meni na vrhu stranice nije centriran u odnosu na logo i ostatak stranice.

 ###### 
D - Validni html i css dokumenti. Kod uredan bez ponavljanja. Sve uradjeno. Prijatne boje i minimalisticki stil. Ljepota. L - Nisam pronasao nista lose. Posto je vec sve dobro dvije preporuke da bude perfektno. Smanjiti razmak izmedju "Spirala 1" i komentara na stranici statistika i centrirat dugmad na stranici unos komentara kada se predje na <700px.
 ###### 
Meni je centriran kao i sadrzaji stranice sto olaksava prelazenje sa jedne na drugu stranicu. Na stranici statistika tekst ispod statistike studenta je nesrazmjeran sa tabelom koja je mnogo veca pa to narusava dizajn stranice. "Ime i prezime" odlutalo je sasvim desno i malim je slovima, umjesto da je u skladu sa fontom i velicinom kao i naziv "statistika studenta".  Pohvalno je sto ima common.css tako da umanji ponavaljanje istog koda. Jako puno je klasa u css-u tipa .naslov, .podnaslov, .comments_title, .comments, .comments_spirala, .page_buttons {...} sto narusava preglednost stranice. Nema vertikalnog scroll-bara. Stranice se ucitavaju, nema bugova, svi zadaci su zavrseni.